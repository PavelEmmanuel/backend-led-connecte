from flask import Flask ,  render_template , g  , request , jsonify
from flask_restful import Resource ,Api
from sqlalchemy import create_engine
import sqlite3
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from werkzeug.exceptions import Unauthorized
import json
from flask_cors import CORS
import os
from datetime import datetime , timedelta

# from threading import Thread

# threads = []

# import paho.mqtt.client as mqtt

# broker = "broker.mqttdashboard.com"

# client = mqtt.Client("clientId-0nvZDVJWL6")

# client.connect(broker , 8000)

# client.subscribe()

led_time = [
    {
        "start" : None,
        "end" : None
    },
   {
        "start" : None,
        "end" : None
    },
   {
        "start" : None,
        "end" : None
    },
   {
        "start" : None,
        "end" : None
    },
   {
        "start" : None,
        "end" : None
    },
   {
        "start" : None,
        "end" : None
    }
]


app = Flask(__name__ )
CORS(app)
DATABASE = 'IOT.db'
e = create_engine('sqlite:///' + DATABASE)
basedir = os.path.abspath(os.path.dirname(__file__))
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, 'IOT.db')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
db = SQLAlchemy(app)
ma = Marshmallow(app)
api = Api(app)


class User(db.Model):
    __tablename__ = 'USER'
    id = db.Column(db.Integer, primary_key=True)
    last_name = db.Column(db.String)
    given_names = db.Column(db.String)
    email = db.Column(db.String)
    password = db.Column(db.String)

    def __init__(self , last_name , given_names , email , password):
        self.last_name = last_name
        self.given_names = given_names
        self.email = email
        self.password = password

class UserSchema(ma.Schema):
    class Meta:
        fields = ('id' ,  'last_name' , 'given_names' , 'email' , 'password')
user_schema = UserSchema()
users_schema = UserSchema(many=True)
class Led(db.Model):
    __tablename__ = 'LED'
    id = db.Column(db.Integer, primary_key=True)
    nom = db.Column(db.String)
    broche = db.Column(db.Integer)
    couleur = db.Column(db.String)
    etat = db.Column(db.Integer)
    id_salle = db.Column(db.Integer)
    number = db.Column(db.Integer)
    running_day= db.Column(db.Integer)
    running_hour= db.Column(db.Integer)
    running_minute= db.Column(db.Integer)
    running_second= db.Column(db.Integer)
    running_time= db.Column(db.DateTime)

    def __init__(self , nom , broche , couleur):
        self.nom = nom
        self.broche = broche
        self.couleur = couleur
        

class LedSchema(ma.Schema):
    class Meta:
        fields = ('id' ,  'nom' , 'etat' , 'broche' , 'couleur' , 'running_day' , 'running_hour' , 'running_minute' , 'running_second' , 'running_time')
led_schema = LedSchema()
leds_schema = LedSchema(many=True)

class LedApi(Resource) :
    def get(self):
        leds = Led.query.all()
        leds = leds_schema.dump(leds)
        return jsonify(leds) 
    def post(self):
        nom = request.json['nom']
        broche = request.json['broche']
        couleur = request.json['couleur']
        led = Led(nom , broche , couleur)
        db.session.add(led)
        db.session.commit()
        led = Led.query.get(led.id)
        return led_schema.jsonify(led)


if __name__ == '__main__':
    app.debug = True
    app.run(host = '0.0.0.0')

def get_db():
    db = getattr(g, '_database', None)
    if db is None:
        db = g._database = sqlite3.connect(DATABASE)
    db.row_factory = sqlite3.Row
    return db

def query_db(query, args=(), one=False):
    cur = get_db().execute(query, args)
    rv = cur.fetchall()
    cur.close()
    return (rv[0] if rv else None) if one else rv



@app.teardown_appcontext
def close_connection(exception):
    db = getattr(g, '_database', None)
    if db is not None:
        db.close()

@app.route('/')
def hello_world():
    return render_template('index.html')

@app.route('/dashboard')
def dashboard():
    return render_template('dashboard.html')

@app.route('/control_center')
def control_center():
    return render_template('control_center.html')


def createThread():
    pass

@app.route('/set_time' , methods=['POST'])
def timer():
    if request.method == 'POST':
        id = request.json['id']
        led = Led.query.get(id)
        index = request.json['index']
        value = request.json['value']
        if value == 1:
            if led.etat != 1:
                led.etat = 1
                led_time[index]['start'] = datetime.now()
                led.running_time = datetime.now()
                db.session.commit()
                return json.dumps({'success' : 'START TIME AJOUTE AVEC SUCCES'})
        else :
            if led.etat != 0 :
                led_time[index]['end'] = datetime.now()
                print(f"start {led_time[index]['start']}")
                print(f"end {led_time[index]['end']}")
                if led_time[index]['start'] is not None:
                    #this is a timedelta
                    t1 = led_time[index]['end'] - led_time[index]['start']
                else:
                    # this is a datetime
                    t1 = led_time[index]['end']  -led.running_time
                
                t0 = timedelta(
                    days = led.running_day,
                    hours = led.running_hour,
                    minutes = led.running_minute,
                    seconds = led.running_second
                )

                t2 = t1  + t0
                led.etat = 0
                led.running_day = t2.days
                led.running_hour = t2.seconds //3600
                led.running_minute = (t2.seconds//60)%60
                led.running_second = ((t2.seconds%3600)%60)
                db.session.commit()
                led_time[index]['start'] = timedelta(seconds =0)
                led_time[index]['end'] =timedelta(seconds =0)
                return led_schema.jsonify(led)
        return json.dumps({'oups' : 'déjà effectue'})

@app.route('/login' , methods=['POST'])
def login():
    email = request.json['email']
    password = request.json['password']
    user = User.query.filter_by(email=email , password=password).first()
    if user:
        return user_schema.jsonify(user)
    else:
        raise Unauthorized

api.add_resource(LedApi , '/leds')
