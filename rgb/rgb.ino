const int redPin =9;
const int bluePin = 10;
const int greenPin = 1;

void setup() {
  // put your setup code here, to run once:

  pinMode(redPin,OUTPUT);
  pinMode(bluePin,OUTPUT);
  pinMode(greenPin, OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  analogWrite(redPin, 50);
  analogWrite(greenPin, 168);
  analogWrite(bluePin, 82);
  delay(10);
}
