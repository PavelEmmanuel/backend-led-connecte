import paho.mqtt.client as mqtt # Import the MQTT library

import time # The time library is useful for delays

broker = "broker.mqttdashboard.com"
client = "clientId-0nvZDVJWL6"
topic = "testtopic/1"

# Our "on message" event

def messageFunction (client, userdata, message):
    topic = str(message.topic)
    message = str(message.payload.decode("utf-8"))
    print(topic + message)

 

client = mqtt.Client(client) # Create a MQTT client object

client.connect(broker, 1883) # Connect to the test MQTT broker

client.subscribe(topic) # Subscribe to the topic AC_unit

client.on_message = messageFunction # Attach the messageFunction to subscription

client.loop_start() # Start the MQTT client

client.publish("AC_unit", "on")


# Main program loop

while(1):
    client.publish("AC_unit", "on") # Publish message to MQTT broker
    time.sleep(1) # Sleep for a second